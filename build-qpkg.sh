#!/bin/bash

QPKG=/sbin/qpkg
QDK_CONF=/etc/config/qdk.conf

log_msg_info () {
echo -e "\e[32m[INFO]\e[0m" $1
}
log_msg_warn () {
echo -e "\e[33m[WARN]\e[0m" $1
}
log_msg_error () {
echo -e "\e[31m[ERRO]\e[0m" $1
}

usage () {
	echo "usage: $(basename $0) [checkenv|setenv|plex]"
}

check_env ()
{
	# check if $QPKG is linked
	if [[ ! -f $QPKG ]]; then
		log_msg_error "$QPKG link does't exit"
	else
		log_msg_info "$QPKG is set up fine."
	fi
	# check if $QDK_CONF is linked
	if [[ ! -f $QDK_CONF ]]; then
		log_msg_error "$QDK_CONF link does't exit"
	else
		log_msg_info "$QDK_CONF is set up fine."
	fi
	log_msg_info "in case of erro: run 'sudo $(basename $0) setenv'"
}

set_env () {
	if [[ $(id -u) -ne 0 ]]; then
		log_msg_error "'$(basename $0) setenv' has to be run as root"
		exit 1
	fi
	# check if $QPKG is linked
	if [[ ! -f $QPKG ]]; then
		log_msg_warn "$QPKG doesn't exist, create it..."
		ln -s ${PWD}${QPKG}.sh  ${QPKG}
	else
		log_msg_info "$QPKG is set up fine."
	fi
	# check if $QDK_CONF is linked
	if [[ ! -f $QDK_CONF ]]; then
		log_msg_error "$QDK_CONF doesn't exist, create it..."
		mkdir -p $(dirname $QDK_CONF)
		ln -s ${PWD}/${QDK_CONF} ${QDK_CONF}
	else
		log_msg_info "$QDK_CONF is set up fine."
	fi
	
}

if [[ $# -eq 0 && $# -gt 1 ]]; then
	log_msg_error "no or too many argument passed!"
	usage
	exit
fi

if [[ $1 == "checkenv" ]]; then
	log_msg_info "check for QNAP env."
	check_env
	exit 0
fi

if [[ $1 == "setenv" ]]; then
	log_msg_info "check for QNAP env."
	set_env
	exit 0
fi

if [[ $1 == "plex" ]]; then
	log_msg_info ""
fi
