    #!/bin/bash

    [ -n "$1" ] || echo "Error: called with no argument" # debug

    qpkg_encrypt(){
            if [ -n "$1" ]; then
                    const1=3589;
                    let "const2=10**9"
                    Qfile="$1"
                    length_Byte=`ls -l $Qfile | awk '{print $5}'`
                    let "val=$length_Byte * $const1 + $const2"
                    echo "$val"
                    let "lenght_Encrypt=$length_Byte - 60"
                    printf "$val" | dd bs=1 seek=$lenght_Encrypt count=10 conv=notrunc of=$Qfile 2>/dev/null
             else
                    echo "internal error: qpkg_encrypt called with no argument"
                    exit 1
       fi
    }

    case $1 in
       --encrypt)
          qpkg_encrypt $2
       ;;
       *)
          echo Usage
       ;;
    esac


